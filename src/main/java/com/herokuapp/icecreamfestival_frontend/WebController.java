package com.herokuapp.icecreamfestival_frontend;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class WebController {

    @GetMapping("/")
    public String index() {
        return "index";
    }

    @GetMapping("/buyermenu")
    public String pembeliMainMenu() {
        return "pembeli-mainmenu";
    }

    @GetMapping("/sellermenu")
    public String penjualMainMenu() {
        return "penjual-mainmenu";
    }

    @GetMapping("/order")
    public String order() {
        return "order";
    }

    @GetMapping("/login")
    public String login() {
        return "login";
    }

    @GetMapping("/signup")
    public String signup() {
        return "signup";
    }

    @GetMapping("/topup")
    public String topup() {
        return "topup";
    }

    @GetMapping("/restock")
    public String restock() {
        return "restock";
    }

}

