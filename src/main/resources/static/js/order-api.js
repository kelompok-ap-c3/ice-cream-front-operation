var root = "https://ice-cream-festival.herokuapp.com";

function getCookie(cname) {
  var name = cname + "=";
  var decodedCookie = decodeURIComponent(document.cookie);
  var ca = decodedCookie.split(';');
  for(var i = 0; i <ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) == ' ') {
      c = c.substring(1);
    }
    if (c.indexOf(name) == 0) {
      return c.substring(name.length, c.length);
    }
  }
  return "";
}

$( document ).ready(function() {
   if (getCookie("role") == "SELLER") {
        $("#linkMenu").attr("href", "/sellermenu");
   }
});

$(function (e) {
    $("#form-order").on('submit', function(event) {
        var data = $("#form-order").serialize().split("&");
        var obj={};
        var role="";
        obj["user"] = getCookie("username");
        for(var key in data)
        {
            obj[data[key].split("=")[0]] = data[key].split("=")[1];
        }
        var jsonData = JSON.stringify(obj);
        console.log(jsonData);
        event.preventDefault();
        $.ajax({
            url: root + "/api/checkout/",
            headers: {
                "Authorization": "Bearer " + getCookie("token")
            },
            data: jsonData,
            method: "POST",
            contentType: "application/json",
            dataType: "json",
            success: function(response) {
                console.log(response);
                var icecream = response.icecream;
                var cost = response.cost.toFixed(2);
                $("#msg-info").text(icecream + " is ready. $" + cost + " has taken from your e-wallet. Let's order again!");
                getStocksList(e);
            },
            error: function (e) {
                $("#msg-info").text("Your previous order was failed. You may choose a scoop and add toppings again.")
                console.log(e.responseText);
                console.log("ERROR : ", e);
            }
        });
    });
    var getStocksList = function(e) {
        $.ajax({
            url : root + "/api/stocks/all",
            dataType:"JSON",
            success : function (response) {
                var i = 0;
                response = response.data;
                while (response[i] != undefined) {
                    $("#qty" + response[i].name).text(response[i].stock);
                    $("#cost" + response[i].name).text(response[i].cost);
                    i += 1;
                }
            }
        });
    };

    getStocksList(e);
});
