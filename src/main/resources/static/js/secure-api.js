var root = "https://ice-cream-festival.herokuapp.com";

function getCookie(cname) {
  var name = cname + "=";
  var decodedCookie = decodeURIComponent(document.cookie);
  var ca = decodedCookie.split(';');
  for(var i = 0; i <ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) == ' ') {
      c = c.substring(1);
    }
    if (c.indexOf(name) == 0) {
      return c.substring(name.length, c.length);
    }
  }
  return "";
}

$( document ).ready(function() {
    $.ajax({
         url : root + "/private/",
         headers: {
             "Authorization": "Bearer " + getCookie("token")
         },
         dataType:"JSON",
         success: function(response) {

         },
         error: function (e) {
            if (e.responseText == "private") {
                $.ajax({
                    url : root + "/api/users/name/" + getCookie("username"),
                    dataType:"JSON",
                    success : function (response) {
                        $("#username").text(getCookie("username"));
                        $("#balance").text(response.balance);
                    }
                });
            } else {
                location.replace("/login");
            }
         }
    });
});

$(function (e) {
    $("#logout").on('click', function(event) {
        document.cookie = "username=";
        document.cookie = "token=";
        document.cookie = "role=";
        location.replace("/");
    });
 });
