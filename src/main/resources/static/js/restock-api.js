var root = "https://ice-cream-festival.herokuapp.com";

function getCookie(cname) {
  var name = cname + "=";
  var decodedCookie = decodeURIComponent(document.cookie);
  var ca = decodedCookie.split(';');
  for(var i = 0; i <ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) == ' ') {
      c = c.substring(1);
    }
    if (c.indexOf(name) == 0) {
      return c.substring(name.length, c.length);
    }
  }
  return "";
}

$( document ).ready(function() {
   if (getCookie("role") == "BUYER") {
        location.replace("/buyermenu");
   }
});

$(function (e) {
    $("#form-restock").on('submit', function(event) {
        var data = $("#form-restock").serialize().split("&");
        var obj={};
        var role="";
        for(var key in data)
        {
            obj[data[key].split("=")[0]] = data[key].split("=")[1];
        }
        var jsonData = JSON.stringify(obj);
        console.log(jsonData);
        event.preventDefault();
        $.ajax({
            url: root + "/api/restocks/",
            headers: {
                "Authorization": "Bearer " + getCookie("token")
            },
            data: jsonData,
            method: "POST",
            contentType: "application/json",
            dataType: "json",
            success: function(response) {
                console.log(response);
                var i = 0;
                var str = "Now you have ";
                for(var key in response)
                {
                    str += ", " + response[key] + " " + key;
                }
                $("#msg-info").text(str);
                getStocksList(e);
            },
            error: function (e) {
                $("#msg-info").text("Your previous restock was failed. Please try again.")
                console.log(e.responseText);
                console.log("ERROR : ", e);
            }
        });
    });

    var getStocksList = function(e) {
        $.ajax({
            url : root + "/api/stocks/all",
            dataType:"JSON",
            success : function (response) {
                var i = 0;
                response = response.data;
                while (response[i] != undefined) {
                    $("#qty" + response[i].name).text(response[i].stock);
                    i += 1;
                }
            }
        });
    };

    getStocksList(e);
});
