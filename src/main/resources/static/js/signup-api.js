var root = "https://ice-cream-festival.herokuapp.com";

$(function (e) {
    $("#form-signup").on('submit', function(event) {
        var data = $("#form-signup").serialize().split("&");
        var obj={};
        for(var key in data)
        {
            console.log(data[key]);
            if (data[key].split("=")[0] == "roles") {
                obj[data[key].split("=")[0]] = data[key].split("=")[1];
            } else if (data[key].split("=")[0] == "email") {
                obj[data[key].split("=")[0]] = data[key].split("=")[1].replace("%40", "@");
            } else {
                obj[data[key].split("=")[0]] = data[key].split("=")[1];
            }
        }
        var jsonData = JSON.stringify(obj);
        console.log(JSON.stringify(obj));
        event.preventDefault();
        $.ajax({
            url: root + "/api/users/",
            data: jsonData,
            method: "POST",
            contentType: "application/json",
            dataType: "json",
            success: function(response) {
                location.replace("/login");
            },
            error: function (e) {
                console.log(e.responseText);
                console.log("ERROR : ", e);
            }
        });
    });
});
