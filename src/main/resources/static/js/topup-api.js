var root = "https://ice-cream-festival.herokuapp.com";

function getCookie(cname) {
  var name = cname + "=";
  var decodedCookie = decodeURIComponent(document.cookie);
  var ca = decodedCookie.split(';');
  for(var i = 0; i <ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) == ' ') {
      c = c.substring(1);
    }
    if (c.indexOf(name) == 0) {
      return c.substring(name.length, c.length);
    }
  }
  return "";
}

$(function (e) {
    $("#form-topup").on('submit', function(event) {
        var obj={};
        obj["name"] = getCookie("username");
        obj["amount"] = $("#amount").val();
        var jsonData = JSON.stringify(obj);
        console.log(jsonData);
        event.preventDefault();
        $.ajax({
            url: root + "/api/topup/",
            headers: {
                "Authorization": "Bearer " + getCookie("token")
            },
            data: jsonData,
            method: "POST",
            contentType: "application/json",
            dataType: "json",
            success: function(response) {
                $("#msg-info").text("Your previous top up was success. You may top up again here.");
            },
            error: function (e) {
                $("#msg-info").text("Your previous top up was failed. You may top up again here.")
                console.log(e.responseText);
                console.log("ERROR : ", e);
            }
        });
    });
});
