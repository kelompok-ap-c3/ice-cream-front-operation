var root = "https://ice-cream-festival.herokuapp.com";

$(function (e) {
    $("#form-login").on('submit', function(event) {
        var data = $("#form-login").serialize().split("&");
        var obj={};
        var role="";
        for(var key in data)
        {
            if (data[key].split("=")[0] == "username") {
                var username = data[key].split("=")[1];
            } else if (data[key].split("=")[0] == "password"){
                var password = data[key].split("=")[1];
            } else {
                var grant_type = data[key].split("=")[1];
            }
        }
        var jsonData = JSON.stringify(obj);
        console.log(JSON.stringify(obj));
        event.preventDefault();
        $.ajax({
            url: root + "/oauth/token?" + 'username=' + username + '&password=' + password + '&grant_type=' + grant_type,
            headers: {
                "Authorization": "Basic " + btoa("my-trusted-client" + ":" + "secret")
            },
            data: jsonData,
            method: "POST",
            contentType: "application/json",
            dataType: "json",
            crossDomain: true,
            success: function(responseLogin) {
                $.ajax({
                     url : root + "/api/users/name/" + username,
                     dataType:"JSON",
                     success: function(responseSearch) {
                        if (username == "admin") {
                            role = "SELLER";
                        } else {
                            role = "BUYER";
                        }
                        document.cookie = "username=" + username
                        document.cookie = "role=" + role;
                        document.cookie = "token=" + responseLogin.access_token;
                        if (role == "BUYER") {
                            location.replace("/buyermenu");
                        } else if (role == "SELLER") {
                            location.replace("/sellermenu");
                        }
                     }
                });
            },
            error: function (e) {
                console.log(e);
                console.log(e.responseText);
                console.log("ERROR : ", e);
            }
        });
    });
});
