//package com.herokuapp.icecreamfestival_frontend;
//
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
//import org.springframework.test.context.junit4.SpringRunner;
//import org.springframework.test.web.servlet.MockMvc;
//import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
//
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
//
//
//@RunWith(SpringRunner.class)
//@WebMvcTest(controllers = WebController.class)
//public class ApplicationTest {
//
//    @Autowired
//    private MockMvc mockMvc;
//
//    @Test
//    public void homePage() throws Exception {
//        mockMvc.perform(MockMvcRequestBuilders.get("/"))
//                .andExpect(status().isUnauthorized());
//    }
//
//    @Test
//    public void buyerMenu() throws Exception {
//        mockMvc.perform(MockMvcRequestBuilders.get("/buyermenu"))
//                .andExpect(status().isUnauthorized());
//    }
//
//    @Test
//    public void sellerMenu() throws Exception {
//        mockMvc.perform(MockMvcRequestBuilders.get("/sellermenu"))
//                .andExpect(status().isUnauthorized());
//    }
//
//    @Test
//    public void order() throws Exception {
//        mockMvc.perform(MockMvcRequestBuilders.get("/order"))
//                .andExpect(status().isUnauthorized());
//    }
//
//    @Test
//    public void login() throws Exception {
//        mockMvc.perform(MockMvcRequestBuilders.get("/login"))
//                .andExpect(status().isOk());
//    }
//
//    @Test
//    public void signUp() throws Exception {
//        mockMvc.perform(MockMvcRequestBuilders.get("/signup"))
//                .andExpect(status().isUnauthorized());
//    }
//
//    @Test
//    public void topup() throws Exception {
//        mockMvc.perform(MockMvcRequestBuilders.get("/topup"))
//                .andExpect(status().isUnauthorized());
//    }
//
//    @Test
//    public void restock() throws Exception {
//        mockMvc.perform(MockMvcRequestBuilders.get("/restock"))
//                .andExpect(status().isUnauthorized());
//    }
//
//}